<?php

/**
 * This is the model class for table "tm_place".
 *
 * The followings are the available columns in table 'tm_place':
 * @property integer $place_id
 * @property string $place_title
 * @property string $place_address
 * @property string $place_map
 * @property string $comment
 * @property string $situation_image
 */
class Place extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tm_place';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('place_title, place_address', 'required'),
			array('place_title, place_address, comment, situation_image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('place_id, place_title, place_address, place_map, comment, situation_image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'place_id' => 'Place',
			'place_title' => 'اسم المكان',
			'place_address' => 'العنوان بالتفصيل',
			'place_map' => 'Place Map',
			'comment' => 'تعليق',
			'situation_image' => 'Situation Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('place_id',$this->place_id);
		$criteria->compare('place_title',$this->place_title,true);
		$criteria->compare('place_address',$this->place_address,true);
		$criteria->compare('place_map',$this->place_map,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('situation_image',$this->situation_image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Place the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
