<?php

$markers= Controller::getMarkers();
?>


<script>
    /*** Google Map Script ***/
    var locations = <?php echo json_encode($markers) ?>;
    console.log(locations);
    var map;
    var latlng;
    function initMap()
    {
        latlng = new google.maps.LatLng(30.089258861504813, 31.18518590927124);
        var myOptions = {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

        var infowindow = new google.maps.InfoWindow({});

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }        
        
    }    
    google.maps.event.addDomListener(window, 'load', initMap);
    
</script>
<div id="map-canvas" style="width:1200px; height:600px; margin:0;padding: 0;" ></div>
