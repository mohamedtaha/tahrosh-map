<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->place_id),array('view','id'=>$data->place_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_title')); ?>:</b>
	<?php echo CHtml::encode($data->place_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_address')); ?>:</b>
	<?php echo CHtml::encode($data->place_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_map')); ?>:</b>
	<?php echo CHtml::encode($data->place_map); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('situation_image')); ?>:</b>
	<?php echo CHtml::encode($data->situation_image); ?>
	<br />


</div>