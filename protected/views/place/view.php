<?php
   list($latitude,$longitude)=  explode(',', $model->place_map); 
?>
<script>
    /****************  Google Map Script *******************/
    var map;
    var latlng;


    function initMap()
    {
        var lat = $('#lat').val();//21.53412;
        var lon = $('#lon').val();//39.18605;
        latlng = new google.maps.LatLng(lat, lon);
        var myOptions = {
            zoom: 18,
            center: latlng
          // mapTypeId: 'satellite'
        };
        map = new google.maps.Map(document.getElementById("view-map"), myOptions);
        //map.setTilt(45);
        console.log(lat);
        console.log(lon);
        // add a click event handler to the map object

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lon),
            map: map
        });
    }

    google.maps.event.addDomListener(window, 'load', initMap);
    /***************END of Google Map SCript ****************/
</script>

<div class="row">
    <div id="place-details" class="span6 pull-right">
        place details are here
    </div>

    <div id="view-map" class="pull-left" style="width:700px; height:400px; margin:0;padding: 0;"></div>

    <input type="hidden" id="lat" value="<?php echo $latitude ?>" >
    <input type="hidden" id="lon" value="<?php echo $longitude ?>"/>
</div>