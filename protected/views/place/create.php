<script>
    // Google Map script
    var map;
    var latlng;
    function initMap()
    {
        latlng = new google.maps.LatLng(30.089258861504813, 31.18518590927124);
        var myOptions = {
            zoom: 12,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("create-map"), myOptions);

        // add a click event handler to the map object

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(30.089258861504813, 31.18518590927124),
            map: map,
            title: "حرك المؤشر",
            draggable: true
        });

        google.maps.event.addListener(marker, "dragend", function(event)
        {
            // place a marker
            //placeMarker(event.latLng);

            // display the lat/lng in your form's lat/lng fields
            document.getElementById("latFld").value = event.latLng.lat();
            document.getElementById("lngFld").value = event.latLng.lng();
        });

    }

    google.maps.event.addDomListener(window, 'load', initMap);

</script>



<div class="row">
    
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'place-form',
    'enableAjaxValidation' => false,
    'htmlOptions'=>array(
        'class'=>'pull-right',
        'style'=>'margin-top:80px'
    )
        ));
?>

    <div class="column span4">
<p class="help-block">الحقول التى يليها <span class="required">*</span> مطلوبة</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'place_title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'place_address', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'comment', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php // echo $form->textFieldRow($model,'situation_image',array('class'=>'span5','maxlength'=>255));  ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'success',
        'label' => $model->isNewRecord ? 'حفظ' : 'إنشاء',
        'htmlOptions'=>array('class'=>'btn-block')
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
    </div>    
<div id="create-map" class="pull-left column" style="width:600px; height:500px; margin:0;padding: 0;" ></div>
<input type="hidden" id="latFld" name="latitude" value="<?php if (isset($latitude)) echo $latitude ?>" >
<input type="hidden" id="lngFld" name="longitude" value="<?php if (isset($longitude)) echo $longitude ?>" >
</div>
<div class="clearfix"></div>