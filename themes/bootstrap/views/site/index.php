<?php

$markers= Controller::getMarkers();
?>

<style type="text/css">
/** FIX for Bootstrap and Google Maps Info window styes problem **/
img[src*="gstatic.com/"], img[src*="googleapis.com/"] {
max-width: none;
}
</style>

<script>
    /*** Google Map Script ***/
    var locations = <?php echo json_encode($markers) ?>;
    console.log(locations);
    var map;
    var latlng;
    function initMap()
    {
        latlng = new google.maps.LatLng(30.089258861504813, 31.18518590927124);
        var myOptions = {
            zoom: 8,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

        var infowindow = new google.maps.InfoWindow({maxWidth: 180});

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                size:20
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent('<center>'+locations[i][0]+'<br><a href="'+'<?php echo CController::createUrl('place/view').'/' ?>'+locations[i][3]+'" target="_blank">تفاصيل أكثر'+'</a>'+'</center>');
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }        
        
    }    
    google.maps.event.addDomListener(window, 'load', initMap);
    
</script>
<div id="map-canvas" style="width:1200px; height:600px; margin:0;padding: 0;" ></div>


